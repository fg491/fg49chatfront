import React, {useState} from 'react';

import {Container} from "react-bootstrap";
import Comments from "./components/Comments";
import FGNavbar from "./components/FGNavbar";
import Statics from "./components/Statics";


function App() {
    const [state, setState] = useState('chat')
    const [messages, setMessages] = useState([])
    return (
        <Container fluid>
            <FGNavbar setState={setState}/>
            {state === 'chat' ? <Comments messages={messages} setMessages={setMessages}/> : <Statics messages={messages} />}
        </Container>
    );
}

export default App;
