import React, {useEffect, useState} from 'react';
import {ListGroup, Spinner} from "react-bootstrap";
import CommentItem from "./CommentItem";

const sock = new WebSocket("ws://localhost:8766")
const Comments = ({messages, setMessages}) => {
    const [load, setLoad] = useState(false) // todo сделать норм
    useEffect(() => {
        sock.onmessage = OnMessage
    }, [])
    const OnMessage = ({data}) => {
        setMessages((messages) => ([JSON.parse(data), ...messages]))
    }
    return <ListGroup>
        {messages ? messages.slice(0, 7).filter(
            (item) => item.type === 'textMessage'
        ).map((item) => {
            const len = messages.filter((i) => i.author.channelId === item.author.channelId).length
            return <CommentItem key={"comment_" + item.id} len={len} data={item}/>
        }) : <p>Пока нет сообщений в чате :(</p>}

    </ListGroup>
}

export default Comments;