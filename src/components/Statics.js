import React from 'react';
import {ListGroup} from "react-bootstrap";
import StaticItem from "./StaticItem";

const groupBy = (xs, f) => {
    return xs.reduce((r, v, i, a, k = f(v)) => ((r[k] || (r[k] = [])).push(v), r), {});
}

const Statics = ({messages}) => {
    const groupMessages = groupBy(messages, (c) => c.author.channelId)
    const items = Object.keys(groupMessages).map(
        (key) => { return [key, groupMessages[key].length] }
    );
    items.sort((first, second) => { return second[1] - first[1] });

    return <ListGroup>
        {messages && items.map((item) => {
            const mgs = groupMessages[item[0]][0]
            return <StaticItem imageUrl={mgs.author.imageUrl} name={mgs.author.name} len={item[1]}/>
        })}</ListGroup>
}

export default Statics;