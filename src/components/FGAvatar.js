import React, {useState} from 'react';
import {Image as BImage} from "react-bootstrap";
import Avarat from "../assert/imgs/avatar.jpg";

const FGAvatar = ({url}) => {
    const [src, setSRC] = useState(url)
    const img = new Image();

    img.src = src
    img.onerror = (e) => {
        setSRC(Avarat)
    }
    return <BImage style={{maxHeight: 80}} src={src} thumbnail roundedCircle/>
}

export default FGAvatar;