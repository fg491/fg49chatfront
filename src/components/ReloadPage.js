import React from 'react';
import {Nav} from "react-bootstrap";
import {ArrowCounterclockwise} from 'react-bootstrap-icons';

const ReloadPage = () => {
    const handlerReload = (event) => {
        event.stopPropagation();
        window.location.reload();
    }
    return <Nav.Link onClick={handlerReload}><ArrowCounterclockwise size={20}/></Nav.Link>
}

export default ReloadPage;