import React from 'react';
import {Badge, Col, ListGroup, Row} from "react-bootstrap";
import FGAvatar from "./FGAvatar";

const StaticItem = ({imageUrl, name, len}) => {
    return <ListGroup.Item>
        <Row style={{minWidth: 91}}>
            <Col xs={2} md={2}>
                <FGAvatar url={imageUrl}/>
            </Col>
            <Col xs={10} md={10} className="d-flex justify-content-between align-items-start">
                <div className="ms-2 me-auto">
                    <div className="fw-bold">{name}</div>
                </div>
                <Badge bg="primary" pill>{len}</Badge>
            </Col>
        </Row>
    </ListGroup.Item>
}

export default StaticItem;