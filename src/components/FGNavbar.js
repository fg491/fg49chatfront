import React from 'react';
import {Container, Nav, Navbar} from "react-bootstrap";
import {ChatDots, FilterCircle} from 'react-bootstrap-icons';
import ReloadPage from "./ReloadPage";

const FGNavbar = ({setState}) => {
    const handlerState = (stateName) => {
        setState(stateName);
    }
    return <Navbar bg="dark" data-bs-theme="dark">
        <Container>
            <Navbar.Brand href="#home">FG49</Navbar.Brand>
            <Nav className="me-auto">
                <ReloadPage/>
                <Nav.Link href="#chat" onClick={() => handlerState('chat')}><ChatDots size={20}/></Nav.Link>
                <Nav.Link href="#static" onClick={() => handlerState('static')}><FilterCircle size={20}/></Nav.Link>
            </Nav>
        </Container>
    </Navbar>
}

export default FGNavbar;